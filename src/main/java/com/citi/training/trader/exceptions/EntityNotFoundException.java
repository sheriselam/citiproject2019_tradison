package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown when a requested entity is not found.
 *
 */
@SuppressWarnings("serial")
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
