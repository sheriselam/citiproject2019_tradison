package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.AverageAndPriceHistory;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.AverageAndPriceHistoryService;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.SimpleStrategyService;
import com.citi.training.trader.service.TradeService;


/**
 * This class representts the logic of the trading algorithm which uses
 * long and short moving averages to decide when to sell or buy
 * @author Rimsa
 */
@Profile("!no-scheduled")
@Component
public class SimpleStrategyAlgorithm implements StrategyAlgorithm {

    private static final Logger logger =
                LoggerFactory.getLogger(SimpleStrategyAlgorithm.class);

    @Autowired
    private TradeSender tradeSender;

    @Autowired
    private PriceService priceService;

    @Autowired
    private SimpleStrategyService strategyService;

    @Autowired
    private TradeService tradeService;
    
    @Autowired
    private AverageAndPriceHistoryService averageAndPriceHistoryService;
    
    //run in every 1 seconds.
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:1000}")
    public void run() {

        for(SimpleStrategy strategy: strategyService.findAll()) {
        	
            Trade lastTrade = null;
 
            try {
                lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
            } catch(EntityNotFoundException ex) {
                logger.debug("No Trades for strategy id: " + strategy.getId());
            }
        	
            
            if(lastTrade != null) {
                if(lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
                    logger.debug("Waiting for last trade to complete, do nothing");
                    continue;
                }

                // account for last trade?
                if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
                    logger.debug("Accounting for Trade: " + lastTrade);
                    if(!strategy.hasPosition()) {
                        if(lastTrade.getTradeType() == Trade.TradeType.SELL) {
                            logger.debug("Confirmed short position for strategy: " + strategy);
                            strategy.takeShortPosition();
                        }
                        else {
                            logger.debug("Confirmed long position for strategy: " + strategy);
                            strategy.takeLongPosition();
                        }
                        strategy.setLastTradePrice(lastTrade.getPrice());
                    }
                    else if(strategy.hasLongPosition()) {
                        logger.debug("Closing long position for strategy: " + strategy);
                        logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                                     lastTrade.getPrice());
                        closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
                    }
                    else if(strategy.hasShortPosition()) {
                        logger.debug("Closing short position for strategy: " + strategy);
                        logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                                     lastTrade.getPrice());
                        closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
                    }
                }

                lastTrade.setAccountedFor(true);
                
                tradeService.save(lastTrade);
            }
            
            // closing them if necessary and go to next stock
            if((strategy.getStopped() != null) && (strategy.hasPosition() == false )) {
            	
            	strategyService.save(strategy);
                continue;
            }
          
            logger.debug("Taking strategic action.");
            
            //TWO MOVING AVERAGE ALGORITHMN
            if(!strategy.hasPosition()) {
            	
            
            	int longwindow = 100;
            	int shortwindow = 15;
            	
            	List<Price> longprices = priceService.findLatest(strategy.getStock(), longwindow); 
            	List<Price> shortprices = priceService.findLatest(strategy.getStock(), shortwindow);
            	
            	if(longprices.size()< longwindow ) {
            		
            		logger.debug("Cannot execute the strategy, not enough price data: " + strategy);
            		continue;
            	}
            	
            	Double longAverage;
            	Double shortAverage;
            	
            	
            	//find long and short average
            	longAverage = findAverage(longprices, longwindow);
             	shortAverage = findAverage(shortprices, shortwindow);
             	
             	// for AverageAndPriceHistory
             	AverageAndPriceHistory aaph = new AverageAndPriceHistory(-1,
             			longAverage, shortAverage, shortprices.get(0).getPrice(),strategy);
             	averageAndPriceHistoryService.save(aaph);
             	
             	if( (strategy.getShortAverage() == 0.0) && (strategy.getLongAverage() == 0.0)) {
             		
             		strategy.setLongAverage(longAverage);
                 	strategy.setShortAverage(shortAverage);
                    strategyService.save(strategy);
                    logger.debug("First enter, take no action");
             		continue;
             	}
                
             	//Check crossover and take position.
             	if((shortAverage >= longAverage) &&
                 			(strategy.getShortAverage() < strategy.getLongAverage())) {
                 		
                 		logger.debug("Trading to close long position for strategy: " + strategy);
                        makeTrade(strategy, Trade.TradeType.BUY);
                 	}
                 	
                 else if ((shortAverage <= longAverage) &&
                 			(strategy.getShortAverage() > strategy.getLongAverage())){
                 		
                 		logger.debug("Trading to close short position for strategy: " + strategy);
                        makeTrade(strategy, Trade.TradeType.SELL);
                 	}
                 	
                 else {
                	    strategy.setLongAverage(longAverage);
                  	    strategy.setShortAverage(shortAverage);
                  	    strategyService.save(strategy);
                  	    
                 		logger.debug("Insufficient price change, take no action");
                 		continue;
                 	}
             	
             	strategy.setLongAverage(longAverage);
             	strategy.setShortAverage(shortAverage);
             	    		
             	
            } else if(strategy.hasLongPosition()) {
                // we have a long position => close the position by selling
                logger.debug("Trading to close long position for strategy: " + strategy);
                makeTrade(strategy, Trade.TradeType.SELL);
            } else if(strategy.hasShortPosition()) {
                // we have a short position => close the position by buying
                logger.debug("Trading to close short position for strategy: " + strategy);
                makeTrade(strategy, Trade.TradeType.BUY);
            }
            
            //strategy.setLongAverage(longAverage);
         	//strategy.setShortAverage(shortAverage);
            strategyService.save(strategy);
        }
    }

    private void closePosition(double profitLossPerShare, SimpleStrategy strategy) {
    	double totalProfitLoss = profitLossPerShare * strategy.getSize();
        logger.debug("Recording profit/loss of: " + totalProfitLoss +
                     " for strategy: " + strategy);
        strategy.addProfitLoss(totalProfitLoss);
        strategy.closePosition();

        if(strategy.getProfit() >= strategy.getExitProfitLoss()) {
            logger.debug("Exit condition reached, exiting strategy");
            strategy.setStopped(new Date());
            strategyService.save(strategy);
        }
    }


    protected double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(currentPrice.getPrice(),
                                        strategy.getSize(), tradeType,
                                        strategy));
        return currentPrice.getPrice();
    }
    
    private double findAverage(List<Price> prices, int WindownLen) {
    	double priceTotal = 0.0;
    	double Average;
    	for (Price i : prices) {
    		priceTotal = priceTotal + i.getPrice();
     		}
     	Average = priceTotal/WindownLen;
     	return Average;
     	
     	
    }

}
