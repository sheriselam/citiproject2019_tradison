package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.AverageAndPriceHistory;
import com.citi.training.trader.service.AverageAndPriceHistoryService;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/averageAndPriceHistory")
public class AverageAndPriceHistoryController {
	
	private static final Logger LOG =
            LoggerFactory.getLogger(AverageAndPriceHistoryController.class);
	
	@Autowired
    private AverageAndPriceHistoryService averageAndPriceHistoryService;
	
	@RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AverageAndPriceHistory> findAll() {
		LOG.info("findAll()");
		return averageAndPriceHistoryService.findAll();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AverageAndPriceHistory> findByStrategyId(@PathVariable int id) {
		LOG.info("findByStrategyId [" + id + "]");
		return averageAndPriceHistoryService.findByStrategyId(id);
	}

}
