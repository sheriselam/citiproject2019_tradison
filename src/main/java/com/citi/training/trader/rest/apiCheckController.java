package com.citi.training.trader.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/status")
public class apiCheckController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String apiCheckController(){
		return "Status OK";
	}

}
