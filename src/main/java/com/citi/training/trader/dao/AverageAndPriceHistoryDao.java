package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.AverageAndPriceHistory;
import com.citi.training.trader.model.Trade;

public interface AverageAndPriceHistoryDao extends CrudRepository<AverageAndPriceHistory, Integer> {

	List<AverageAndPriceHistory> findByStrategy_Id(int id);
}
