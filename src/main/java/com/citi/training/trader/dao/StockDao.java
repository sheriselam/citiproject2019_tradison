package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Stock;

public interface StockDao extends CrudRepository<Stock, Integer> {

    Stock findByTicker(String ticker);

}
