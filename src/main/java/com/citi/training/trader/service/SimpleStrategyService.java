package com.citi.training.trader.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.google.common.collect.Lists;

@Component
public class SimpleStrategyService {

    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return Lists.newArrayList(simpleStrategyDao.findAll());
    }

    public SimpleStrategy save(SimpleStrategy strategy) {
        return simpleStrategyDao.save(strategy);
    }

    public void deleteById(int id) {
        simpleStrategyDao.deleteById(id);
    }
    
    public SimpleStrategy stop(int id) {
    	SimpleStrategy strategy = simpleStrategyDao.findById(id).get();
    	strategy.setStopped(new Date());
    	return simpleStrategyDao.save(strategy);
    }
    
    public SimpleStrategy update(SimpleStrategy strategy) {
    	SimpleStrategy dbStrategy = simpleStrategyDao.findById(strategy.getId()).get();
    	
    	dbStrategy.setStopped(strategy.getStopped());
    	
    	simpleStrategyDao.save(dbStrategy);    

    	return dbStrategy;
    }
}
