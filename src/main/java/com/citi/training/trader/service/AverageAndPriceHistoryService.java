package com.citi.training.trader.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.AverageAndPriceHistoryDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.AverageAndPriceHistory;
import com.google.common.collect.Lists;


@Component
public class AverageAndPriceHistoryService {
	
	private static final Logger LOG =
            LoggerFactory.getLogger(AverageAndPriceHistoryService.class);
	
	@Autowired
    private AverageAndPriceHistoryDao averageAndPriceHistoryDao;

    public List<AverageAndPriceHistory> findAll(){
        return Lists.newArrayList(averageAndPriceHistoryDao.findAll());
    }

    public AverageAndPriceHistory save(AverageAndPriceHistory aaph) {
        return averageAndPriceHistoryDao.save(aaph);
    }
  
    public List<AverageAndPriceHistory> findByStrategyId(int id) {
        try {
            return averageAndPriceHistoryDao.findByStrategy_Id(id);
        } catch(NoSuchElementException ex) {
            String msg = "Trade not found: findById [" + id + "]";
            LOG.warn(msg);
            LOG.warn(ex.toString());
            throw new EntityNotFoundException(msg);
        }
    }

}
