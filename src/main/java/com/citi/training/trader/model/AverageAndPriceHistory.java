package com.citi.training.trader.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AverageAndPriceHistory {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String recordDate;
	private double longAvg;
    private double shortAvg;
    private double price;
    
    @ManyToOne
    private SimpleStrategy strategy;
    
    public AverageAndPriceHistory() {}
    
    public AverageAndPriceHistory(int id, double longAvg, double shortAvg, double price, SimpleStrategy strategy) {
    	this.id = id;
    	
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
    	LocalDateTime now = LocalDateTime.now();  
    	this.recordDate = dtf.format(now);
    	
    	this.longAvg = longAvg;
    	this.shortAvg = shortAvg;
    	this.price = price;
    	this.strategy = strategy;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}

	public double getLongAvg() {
		return longAvg;
	}

	public void setLongAvg(double longAvg) {
		this.longAvg = longAvg;
	}

	public double getShortAvg() {
		return shortAvg;
	}

	public void setShortAvg(double shortAvg) {
		this.shortAvg = shortAvg;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public SimpleStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(SimpleStrategy strategy) {
		this.strategy = strategy;
	}
}
