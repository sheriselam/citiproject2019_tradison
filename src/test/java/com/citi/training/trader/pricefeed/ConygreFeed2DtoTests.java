package com.citi.training.trader.pricefeed;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ConygreFeed2DtoTests {
	
	private String testSymbol = "symbol";
    private double testPrice = 50.5;
    private String testTimeStamp = "12.30";
    private String testCompanyName = "Apple";
    private int testPeriodNumber = 66;

	@Test
    public void test_ConygreFeed_toString() {
        ConygreFeed2Dto testConygreFeed  = new ConygreFeed2Dto();
        testConygreFeed.setSymbol(testSymbol);
        testConygreFeed.setPrice(testPrice);
        testConygreFeed.setTimeStamp(testTimeStamp);
        testConygreFeed.setCompanyName(testCompanyName);
        testConygreFeed.setPeriodNumber(testPeriodNumber);

        assertTrue("Testing if the string contains the right symbol",
                   testConygreFeed.toString().contains(testConygreFeed.getSymbol()));
        assertTrue("Testing if the string contains the right price",
                testConygreFeed.toString().contains(Double.toString(testConygreFeed.getPrice())));
        assertTrue("Testing if the string contains the right timestamp",
                testConygreFeed.toString().contains(testConygreFeed.getTimeStamp()));
        assertTrue("Testing if the string contains the right company name",
                testConygreFeed.toString().contains(testConygreFeed.getCompanyName()));
        assertTrue("Testing if the string contains the right period number",
                testConygreFeed.toString().contains(Integer.toString(testConygreFeed.getPeriodNumber())));

    }
}
