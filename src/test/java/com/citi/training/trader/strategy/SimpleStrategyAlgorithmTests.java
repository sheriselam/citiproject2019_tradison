package com.citi.training.trader.strategy;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade.TradeType;
import com.citi.training.trader.service.PriceService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class SimpleStrategyAlgorithmTests {
	
	private double testProfitLossPerShare = 50.0;
	private Stock testStock = new Stock(5, "Apple");
	private Date dateobj = new Date();

	private double testPrice = 50.55;
	private SimpleStrategy testStrategy = new SimpleStrategy(5, testStock, 100, 31.3, 77, testPrice,200,
			dateobj, 55.5, 92.4 );
	//profit is 200
	//exit profit loss is 31.3
	
	@Autowired
	SimpleStrategyAlgorithm testAlgorithm;
	
	@MockBean
    private TradeSender tradeSender;

    @MockBean
    private PriceService priceService;
	
	@Test
	public void test_closePosition() {
		double testTotalProfitLoss = testProfitLossPerShare * testStrategy.getSize();
		assertEquals("Checking the correct product of ProfitLoss per share and strategy size is found",
				testTotalProfitLoss, 5000.00, 0.01);
		assertEquals("Checking if strategy was stopped on the date set in constructor",
				dateobj.toString(),testStrategy.getStopped().toString());
	}
	
	@Test
	public void test_makeTrade() {	
		Price testPriceObj = new Price(testStock,testPrice,dateobj);
		List<Price> sampleprices = new ArrayList<Price>();
		sampleprices.add(testPriceObj);
		
		when(priceService.findLatest(testStock, 1)).thenReturn(sampleprices);
		assertEquals("Checking if makeTrade returns the same price as the array",
				sampleprices.get(0).getPrice(), testAlgorithm.makeTrade(testStrategy,
				TradeType.SELL), 0.01);
	}
	
//	//if trade type is sell does strategy take short position?
//	@Test
//	public void test_run() {
//		Trade testTrade = new Trade(testPrice, testSize, TradeType.SELL, testStrategy);
//		assertTrue(testTrade.getStrategy().hasShortPosition());
//		
//	}

}
