package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class SimpleStrategyTests {

    private int testId = 444;
    private String testTicker = "GOOG";
    private Stock testStock = new Stock(12, "GOOG");
    private int testSize = 5999;
    private double testExitProfitLoss = 100.99;
    private int testCurrentPosition = 1;
    private double testLastTradePrice = 12.48;
    private double testProfit = 39.99;
    private Date testStopped = new Date();
    private double testLongAverage = 41.2;
    private double testShortAverage = 87.1;

    @Test
    public void test_SimpleStrategy_fullConstructor() {
        SimpleStrategy testSimpleStrategy = new SimpleStrategy(
                                testId, testStock, testSize, testExitProfitLoss,
                                testCurrentPosition, testLastTradePrice, testProfit,
                                testStopped, testLongAverage, testShortAverage);

        assertEquals("testStrategy should have id that was set in constructor",
                     testId, testSimpleStrategy.getId());

        assertEquals("testStrategy should have Stock ticker that was set in constructor",
                     testTicker.toLowerCase(), testSimpleStrategy.getStock().getTicker());

        assertEquals("testStrategy should have size that was set in constructor",
                     testSize, testSimpleStrategy.getSize());

        assertEquals("testStrategy should have exit P&L that was set in constructor",
                     testExitProfitLoss, testSimpleStrategy.getExitProfitLoss(), 0.0001);

        assertTrue("testStrategy should have Long Position",
                   testSimpleStrategy.hasLongPosition());

        assertTrue("testStrategy should have Long Position",
                   testSimpleStrategy.hasPosition());

        assertEquals("testStrategy should have lastTradePrice that was set in constructor",
                     testLastTradePrice, testSimpleStrategy.getLastTradePrice(), 0.0001);
    }
}
