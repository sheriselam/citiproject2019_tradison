package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class AverageAndPriceHistoryTests {
	
	private int testId = 444;
	private double testLongAvg = 46.7;
	private double testShortAvg = 77.9;
	private double testPrice = 50.1;
	
	private SimpleStrategy testStrategy = new SimpleStrategy();
	
	@Test
    public void test_AverageAndPriceHistory_constructor() { 
		AverageAndPriceHistory testAverageAndPriceHistory = new AverageAndPriceHistory(
				testId, testLongAvg, testShortAvg, testPrice, testStrategy);
			assertEquals(testId, testAverageAndPriceHistory.getId());
			assertEquals(testLongAvg, testAverageAndPriceHistory.getLongAvg(),0.01);
			assertEquals(testShortAvg, testAverageAndPriceHistory.getShortAvg(), 0.01);
			assertEquals(testPrice, testAverageAndPriceHistory.getPrice(), 0.01);
			assertEquals(testStrategy, testAverageAndPriceHistory.getStrategy());
		
	}	
}
