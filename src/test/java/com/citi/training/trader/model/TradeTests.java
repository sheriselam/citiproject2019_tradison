package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

public class TradeTests {

    private int defaultId = -1;
    private double testPrice = 99.99;
    private int testTradeSize = 1500;
    private Trade.TradeType testTradeType = Trade.TradeType.BUY;
    private Trade.TradeState testTradeState = Trade.TradeState.DONE_FOR_DAY;
    private SimpleStrategy testSimpleStrategy = new SimpleStrategy();
    private boolean testAccountedFor = true;
    private LocalDateTime testLocalDateTime = LocalDateTime.now();

    /**
     * Checking if the trade contains all the parameters from the constructor
     * :ID, price, size and trade type
     */
    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testPrice, testTradeSize,
                                    testTradeType, testSimpleStrategy);

        assertEquals("Create Trade should contain default id of -1",
                     defaultId, testTrade.getId().intValue());

        assertEquals("Created Trade should contain price given in constructor",
                     testPrice, testTrade.getPrice(), 0.00001);

        assertEquals("Created Trade should contain size given in constructor",
                     testTradeSize, testTrade.getSize());

        assertEquals("Created Trade should contain trade type given in constructor",
                     testTradeType, testTrade.getTradeType());
    }
    
    @Test
    public void test_Trade_constructor2() {
        Trade testTrade = new Trade(defaultId, testPrice, testTradeSize,
                testTradeType, testTradeState,testLocalDateTime, testAccountedFor,
                testSimpleStrategy);

        assertEquals("Create Trade should contain default id",
                     defaultId, testTrade.getId().intValue());

        assertEquals("Created Trade should contain price given in constructor",
                     testPrice, testTrade.getPrice(), 0.00001);

        assertEquals("Created Trade should contain size given in constructor",
                     testTradeSize, testTrade.getSize());

        assertEquals("Created Trade should contain trade type given in constructor",
                     testTradeType, testTrade.getTradeType());
        
        assertEquals("Created Trade should contain trade state type given in constructor",
                	testTradeState, testTrade.getState());
        
        assertEquals("Created Trade should contain last state change given in constructor",
        			testLocalDateTime, testTrade.getLastStateChange());
        
        assertEquals("Created Trade should contain if its accounted for given in constructor",
    			testAccountedFor, testTrade.getAccountedFor());
        
        assertEquals("Created Trade should contain strategy given in constructor",
    				testSimpleStrategy, testTrade.getStrategy());
    }

    /**
     * Checking that the XML strings are converted to the right Trade states
     * "BUY" or "SELL"
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_TradeType_fromXmlString() {
        assertEquals("The XML String \"true\" should be converted to TradeType BUY",
                     Trade.TradeType.BUY, Trade.TradeType.fromXmlString("true"));

        assertEquals("The XML String \"false\" should be converted to TradeType SELL",
                     Trade.TradeType.SELL, Trade.TradeType.fromXmlString("false"));
    
        Trade.TradeType.fromXmlString("TTRUE");
    }
}
